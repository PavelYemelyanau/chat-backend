import bcrypt from 'bcrypt';

export const comparePassword = (
  requestPassword: string,
  userPassword: string
): boolean => bcrypt.compareSync(requestPassword, userPassword);

export const hashPassword = (password: string): string =>
  bcrypt.hashSync(password, 10);
