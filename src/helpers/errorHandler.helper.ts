import { Response } from 'express';

export const errorHadnler = (res: Response, error: Error): void => {
  res.status(505).json({
    message: error.message ? error.message : error,
  });
};
