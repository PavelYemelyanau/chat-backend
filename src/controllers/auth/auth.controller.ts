import { createUser, login } from './../../services/auth.service';
import { IUser } from './../../interfaces/user/user.interface';
import { errorHadnler } from './../../helpers/errorHandler.helper';
import { Request, Response } from 'express';

export const signIn = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const token = await login(email, password);
  if (typeof token === 'string') {
    res.status(201).json({
      token,
    });
  } else {
    res.status(401).json(token.message);
  }
};

export const signUp = async (req: Request, res: Response): Promise<void> => {
  const body = req.body as IUser;
  const token = await createUser(body);
  if (typeof token === 'string') {
    res.status(201).json({ token });
  } else {
    res.status(409).json({
      message: token.message,
    });
  }
};
