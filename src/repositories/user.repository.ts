import { hashPassword } from './../helpers/bcrypt.helper';
import { IUser } from './../interfaces/user/user.interface';
import User from './../models/user/user.model';

export const createNewUser = async (user: IUser): Promise<IUser> => {
  const newUser = new User({
    firstName: user.firstName,
    lastName: user.lastName,
    gender: user.gender,
    nickName: user.nickName,
    email: user.email,
    password: hashPassword(user.password),
  });
  console.log(newUser);
  await newUser.save();
  return newUser;
};

export const findUserByEmail = async (email: string): Promise<IUser> => {
  const user = await User.findOne({ email });
  return user ? user : null;
};
