import { IUser } from './../../interfaces/user/user.interface';
import mongoose, { Schema } from 'mongoose';

const UserSchema: Schema = new Schema<IUser>({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    required: true,
    enum: ['Female', 'Male'],
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
  },
  nickName: {
    type: String,
    minlength: 2,
    required: true,
  },
});

const User = mongoose.model<IUser>('user', UserSchema);
export default User;
