import jwt from 'jsonwebtoken';

export const createToken = (email: string, nickName: string): string =>
  `Bearer ${jwt.sign({ email, nickName }, process.env.SECRETKEY)}`;
