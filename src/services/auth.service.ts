import e from 'express';
import { comparePassword } from './../helpers/bcrypt.helper';
import { IUser } from './../interfaces/user/user.interface';
import {
  createNewUser,
  findUserByEmail,
} from './../repositories/user.repository';
import { createToken } from './token.service';

export const createUser = async (user: IUser): Promise<string | Error> => {
  const checkOnExist = await findUserByEmail(user.email);
  if (checkOnExist) {
    return new Error('User with this email already exist');
  }
  const newUser = await createNewUser(user);
  const token = createToken(user.email, user.nickName);
  return token;
};

export const login = async (
  email: string,
  password: string
): Promise<string | Error> => {
  const user = await findUserByEmail(email);
  if (user) {
    const compare = comparePassword(password, user.password);
    if (compare) {
      return createToken(email, password);
    }
  }
  return new Error('Invalid email or password');
};
