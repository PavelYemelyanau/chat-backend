import bodyParser from 'body-parser';
import express from 'express';
import { connection } from './connection/connection';
import dotenv from 'dotenv';
import routes from './routes/index';

dotenv.config();
const app = express();
const PORT: number = +process.env.PORT || 5000;

const db: string = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.baifv.gcp.mongodb.net/enemychat?retryWrites=true&w=majority`;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

connection(db);

app.use('/api', routes);

app.listen(PORT, () => {
  console.log(`Server started on http://localhost:${PORT}`);
});
